//
//  ViewController.swift
//  OpenWeather
//
//  Created by Master Móviles on 4/10/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate{

    @IBOutlet weak var weatherImg: UIImageView!
    @IBOutlet weak var weatherNow: UILabel!
    @IBOutlet weak var localText: UITextField!
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //código del método
        if let hola = Int(string){
            return(false)
        }
        else{
            return(true)
        }
    }
    
    @IBAction func weatherRequest(_ sender: AnyObject) {
        consultarTiempo(localidad: localText.text!)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localText.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    let OW_URL_BASE = "http://api.openweathermap.org/data/2.5/weather?lang=es&units=metric&appid=1adb13e22f23c3de1ca37f3be90763a9&q="
    let OW_URL_BASE_ICON = "http://openweathermap.org/img/w/"
    
    func consultarTiempo(localidad:String) {
        let urlString = OW_URL_BASE+localidad
        let url = URL(string:urlString)
        let dataTask = URLSession.shared.dataTask(with: url!) {
        
            datos, respuesta, error in
        do {
            let jsonStd = try JSONSerialization.jsonObject(with: datos!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String:AnyObject]
            let weather = jsonStd["weather"]! as! [AnyObject]
            let currentWeather = weather[0] as! [String:AnyObject]
            let descripcion = currentWeather["description"]! as! String
            print("El tiempo en \(localidad) es: \(descripcion)")
            //Estamos bajándonos la imagen pero todavía no la usamos
            let icono = currentWeather["icon"]! as! String
            if let urlIcono = URL(string: self.OW_URL_BASE_ICON+icono+".png" ) {
                let datosIcono = try Data(contentsOf: urlIcono)
                let imagenIcono = UIImage(data: datosIcono)
                self.weatherImg.image = imagenIcono
            }
            self.weatherNow.text = descripcion
        }catch{
            print("error")
        }
        }
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        sleep(2)
               dataTask.resume()
    }
}

